package com.arturorsmd.retrofit;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;


import com.arturorsmd.retrofit.Models.JsonPlaceHolder;
import com.arturorsmd.retrofit.RetrofitSetup.JsonPlaceHolderService;
import com.arturorsmd.retrofit.RetrofitSetup.RetrofitClient;

import java.io.IOException;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new getJsonPlaceHolderDataSync().execute();
        //getJsonPlaceHolderDataAsync();
        //getPostById(2);

    }

    public static void getJsonPlaceHolderDataAsync(){

        Retrofit retrofit = RetrofitClient.getRetrofitClient("https://jsonplaceholder.typicode.com/");
        JsonPlaceHolderService jsonPlaceHolderService = retrofit.create(JsonPlaceHolderService.class);
        Call<List<JsonPlaceHolder>> jsonPlaceHolderCall = jsonPlaceHolderService.getPostList();
        jsonPlaceHolderCall.enqueue(new Callback<List<JsonPlaceHolder>>() {
            @Override
            public void onResponse(Call<List<JsonPlaceHolder>> call, Response<List<JsonPlaceHolder>> response) {
                for (JsonPlaceHolder data: response.body()){
                    Log.e("Title", data.getTitle());
                }

            }

            @Override
            public void onFailure(Call<List<JsonPlaceHolder>> call, Throwable t) {
                Log.e("Data Response", t.getMessage());
            }
        });

    }

    public static class getJsonPlaceHolderDataSync extends AsyncTask<Void, Void, Void> {

        Retrofit retrofit = RetrofitClient.getRetrofitClient("https://jsonplaceholder.typicode.com/");
        JsonPlaceHolderService jsonPlaceHolderService = retrofit.create(JsonPlaceHolderService.class);
        Call<List<JsonPlaceHolder>> jsonPlaceHolderCall = jsonPlaceHolderService.getPostList();

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                for (JsonPlaceHolder data: jsonPlaceHolderCall.execute().body()){
                    Log.e("DataResponse", data.getTitle());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public static void getPostById(int id){
        Retrofit retrofit = RetrofitClient.getRetrofitClient("https://jsonplaceholder.typicode.com/");
        JsonPlaceHolderService jsonPlaceHolderService = retrofit.create(JsonPlaceHolderService.class);
        Call<List<JsonPlaceHolder>> jsonPlaceHolderCall = jsonPlaceHolderService.getPostById(id);

        jsonPlaceHolderCall.enqueue(new Callback<List<JsonPlaceHolder>>() {
            @Override
            public void onResponse(Call<List<JsonPlaceHolder>> call, Response<List<JsonPlaceHolder>> response) {

                for (JsonPlaceHolder data: response.body()){
                    Log.e("DataResponse", data.getTitle());

                }
            }

            @Override
            public void onFailure(Call<List<JsonPlaceHolder>> call, Throwable t) {

            }
        });
    }

}


