package com.arturorsmd.retrofit.RetrofitSetup;

import com.arturorsmd.retrofit.Models.JsonPlaceHolder;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface JsonPlaceHolderService {

    @GET ("posts")
    Call<List<JsonPlaceHolder>> getPostList();

    @GET ("posts/")
    Call<List<JsonPlaceHolder>> getPostById(@Query("userId") int id);

}
